from django.urls import path
from .views import todo_list_view, todo_list_detail_view, todo_list_update_view
from . import views

app_name = 'todos'
urlpatterns = [
    path('', todo_list_view, name='todo_list_list'),
    path('<int:id>/', todo_list_detail_view, name='todo_list_detail'),
    path('create/', views.todo_list_create_view, name='todo_list_create'),
    path('<int:id>/edit/', todo_list_update_view, name='todo_list_update'),
]
